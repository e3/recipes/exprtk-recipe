# exprtk conda recipe

Home: "https://github.com/icshwi/exprtk-ess/exprtk"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS exprtk module
